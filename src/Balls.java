import java.util.Arrays;
//http://learningviacode.blogspot.com.ee/2013/05/sorting-enums-in-java.html
public class Balls {

   enum Color {green, red}

   public static void main(String[] param) {

      Color[] balls = new Color[10];
      int rCount = 0;
      for (int i = 0; i < balls.length; i++) {
         if (Math.random() < 0.5) {
            balls[i] = Balls.Color.red;
            rCount++;
         } else {
            balls[i] = Balls.Color.green;
         }
      }
      reorder(balls);

      System.out.println(Arrays.toString(balls));
   }


   public static void reorder(Color[] balls) {
      int all = 0;
      int red = all;

      //if red annab väärtus 1, siis green 0
      for (Color c : balls)
         //loeb punaste pallide arv ja omistab sellega "all"
         //taidab jarjest punastega massiivi indeksi järgi
         if (c.ordinal() > 0)
            all++;
      for (red = 0; red < all; red++) {
         balls[red] = Color.red;
      }
      //tyhjad kohad taidab rohelistega
      for (all = red; all < balls.length; all++) {
         balls[all] = Color.green;
      }
   }
}